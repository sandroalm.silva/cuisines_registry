package de.quandoo.recruitment.registry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {
	private Map<String, Cuisine> cuisineToCustomers = new HashMap<>();

	@Override
	public void register(final Customer customer, final Cuisine cuisine) {
		final String name = cuisine.getName();
		Cuisine cusineCustomers = cuisineToCustomers.getOrDefault(name, new Cuisine(name));
		cusineCustomers.add(customer);
		cuisineToCustomers.put(name, cusineCustomers);
	}

	@Override
	public List<Customer> cuisineCustomers(final Cuisine cuisine) {
		final String name = cuisine.getName();
		Cuisine cusineCustomers = cuisineToCustomers.getOrDefault(name, new Cuisine(name));
		return cusineCustomers.getCustomers();
	}

	@Override
	public List<Cuisine> customerCuisines(final Customer customer) {
		return cuisineToCustomers.values().stream().filter(c -> c.contains(customer)).collect(Collectors.toList());
	}

	@Override
	public List<Cuisine> topCuisines(final int max) {
		return cuisineToCustomers.values().stream().parallel().sorted(Cuisine.COMPARATOR).limit(max)
				.collect(Collectors.toList());
	}
}
