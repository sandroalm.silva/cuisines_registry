package de.quandoo.recruitment.registry.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Cuisine {

	private final List<Customer> customers;
	private final String name;
	public static final Comparator COMPARATOR = new Comparator();

	public Cuisine(String name) {
		this.name = name;
		this.customers = new LinkedList<>();
	}

	public void add(Customer customer) {
		customers.add(customer);
	}

	public Integer getCustomersAmount() {
		return customers.size();
	}

	public String getName() {
		return name;
	}

	public List<Customer> getCustomers() {
		return new ArrayList<>(customers);
	}

	public boolean contains(Customer customer) {
		return customers.contains(customer);
	}

	@Override
	public String toString() {
		return name.toUpperCase();
	}

	static class Comparator implements java.util.Comparator<Cuisine> {
		@Override
		public int compare(Cuisine c1, Cuisine c2) {
			return c2.getCustomersAmount().compareTo(c1.getCustomersAmount());
		}

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuisine other = (Cuisine) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
