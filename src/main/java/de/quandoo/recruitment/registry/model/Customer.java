package de.quandoo.recruitment.registry.model;

public class Customer {
	private final Long id;
	private final String name;

	public Customer(final Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

}
