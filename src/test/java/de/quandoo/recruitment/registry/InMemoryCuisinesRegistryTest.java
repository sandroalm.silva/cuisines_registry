package de.quandoo.recruitment.registry;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

public class InMemoryCuisinesRegistryTest {

	private Cuisine italian;
	private Cuisine french;
	private Cuisine german;

	private CuisinesRegistry cuisinesRegistry;
	private Customer first;

	@Before
	public void setUp() {
		first = new Customer(1l, "Fist");
		cuisinesRegistry = new InMemoryCuisinesRegistry();
		italian = new Cuisine("ITALIAN");
		german = new Cuisine("GERMAN");
		french = new Cuisine("french");
	}

	@Test
	public void shouldRegisterCustomerOnfrenchCusine() {
		// given
		cuisinesRegistry.register(first, french);

		// when
		List<Customer> result = cuisinesRegistry.cuisineCustomers(french);

		// then
		assertThat(result).isNotEmpty();
		assertThat(result).containsOnly(first);
	}

	@Test
	public void shouldRegisterCustomerOnGermanCusine() {
		// given
		cuisinesRegistry.register(first, german);

		// when
		List<Customer> result = cuisinesRegistry.cuisineCustomers(german);

		// then
		assertThat(result).isNotEmpty();
		assertThat(result).containsOnly(first);
	}

	@Test
	public void shouldReturnCustomersByCusine() {
		// given
		Customer secondCustomer = new Customer(2l, "SecondCustomer");
		cuisinesRegistry.register(first, italian);
		cuisinesRegistry.register(secondCustomer, italian);

		// when
		List<Customer> result = cuisinesRegistry.cuisineCustomers(italian);

		// then
		assertThat(result).isNotEmpty();
		assertThat(result).containsOnly(first, secondCustomer);
	}

	@Test
	public void shouldReturnCusinesByCustomer() {
		// given
		cuisinesRegistry.register(first, italian);
		cuisinesRegistry.register(first, german);

		// when
		List<Cuisine> result = cuisinesRegistry.customerCuisines(first);

		// then
		assertThat(result).isNotEmpty();
		assertThat(result).containsOnly(italian, german);
	}

	@Test
	public void shoudReturnTopCusines() {
		// given
		Customer second = new Customer(2l, "second");
		Customer third = new Customer(3l, "third");

		cuisinesRegistry.register(first, german);
		cuisinesRegistry.register(second, german);
		cuisinesRegistry.register(third, german);

		cuisinesRegistry.register(first, italian);
		cuisinesRegistry.register(second, italian);

		cuisinesRegistry.register(first, french);

		// when
		List<Cuisine> result = cuisinesRegistry.topCuisines(2);

		// then
		assertThat(result).isNotEmpty();
		assertThat(result).containsOnly(italian, german);
	}
}